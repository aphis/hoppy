//
//  Tile.swift
//  Hoppy
//
//  Created by Sashucity on 9/4/16.
//  Copyright © 2016 Sashucity. All rights reserved.
//

import Foundation

struct TilePosition: Equatable {
    var row: Int
    var column: Int
}

func ==(lhs: TilePosition, rhs: TilePosition)-> Bool {
    return lhs.row == rhs.row && lhs.column == rhs.column
}


class Tile {
    var position: TilePosition
    var index: Int
    
    init(position: TilePosition, index: Int) {
        self.position = position
        self.index = index
    }
}