//
//  VideoViewController.swift
//  Hoppy
//
//  Created by Sashucity on 9/5/16.
//  Copyright © 2016 Sashucity. All rights reserved.
//

import UIKit
import AVKit
import AVFoundation

class VideoViewController: UIViewController {
    var gameItem: GameItem!
    var shouldBeDismissed = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        shouldBeDismissed = false
        
        playVideo()
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        if shouldBeDismissed {
            navigationController?.popToRootViewControllerAnimated(true)
        }
    }
    
    func playVideo() {
        if let url = gameItem.videoUrl {
            let player = AVPlayer(URL: url)
            let playerController = AVPlayerViewController()
            playerController.player = player
            
            shouldBeDismissed = true
            
            presentViewController(playerController, animated: true) {
                playerController.player!.play()
            }
        }
    }
}
