//
//  ZoomAnimator.swift
//  Hoppy
//
//  Created by Sashucity on 9/5/16.
//  Copyright © 2016 Sashucity. All rights reserved.
//

import UIKit

class ZoomAnimator: NSObject, UIViewControllerAnimatedTransitioning, UIViewControllerTransitioningDelegate {
    
    func animateTransition(transitionContext: UIViewControllerContextTransitioning) {
        let containerView = transitionContext.containerView()!
        let fromVC = transitionContext.viewControllerForKey(
            UITransitionContextFromViewControllerKey) as! StartViewController
        let btn = fromVC.selectedButton!
        let snapshot = btn.snapshotViewAfterScreenUpdates(false)
        snapshot.center = btn.center
        
        let toVC = transitionContext.viewControllerForKey(
            UITransitionContextToViewControllerKey) as! GameViewController
        fromVC.view.alpha = 1
        toVC.view.alpha = 0
        containerView.addSubview(toVC.view)
        containerView.addSubview(snapshot)
        
        let duration = transitionDuration(transitionContext)
        UIView.animateWithDuration(duration, animations: {
            fromVC.view.alpha = 0
            toVC.view.alpha = 1
            
            snapshot.bounds = toVC.imageView.bounds
            snapshot.center = toVC.view.center
            }, completion: { finished in
                let cancelled = transitionContext.transitionWasCancelled()
                transitionContext.completeTransition(!cancelled)
                
                fromVC.view.alpha = 1
                
                snapshot.removeFromSuperview()
        })
    }
    
    func transitionDuration(transitionContext: UIViewControllerContextTransitioning?)-> NSTimeInterval {
        return 0.4
    }
    
    func animationControllerForPresentedController(presented: UIViewController, presentingController presenting: UIViewController, sourceController source: UIViewController)-> UIViewControllerAnimatedTransitioning? {
        return self
    }
    
    func animationControllerForDismissedController(dismissed: UIViewController)-> UIViewControllerAnimatedTransitioning? {
        return self
    }
}
