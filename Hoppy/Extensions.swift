//
//  Extensions.swift
//  Hoppy
//
//  Created by Sashucity on 9/4/16.
//  Copyright © 2016 Sashucity. All rights reserved.
//

import UIKit

extension UIImage {
    func tileImages(countInRow: Int)-> [UIImage] {
        var images = [UIImage]()
        
        let dx: CGFloat = 2.0
        let dy: CGFloat = 2.0
        
        let tileWidth = ceil(size.width / CGFloat(countInRow))
        
        var y: CGFloat = 0
        
        for _ in 0..<countInRow  {
            var x: CGFloat = 0
            for _ in 0..<countInRow {
                let rect = CGRectInset(CGRectMake(x, y, tileWidth, tileWidth), dx, dy)
                let img = CGImageCreateWithImageInRect(self.CGImage, rect)!
                
                images.append(UIImage(CGImage: img))
                
                x += tileWidth
            }
            
            y += tileWidth
        }
        
        return images
    }
}
