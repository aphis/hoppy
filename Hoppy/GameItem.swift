//
//  GameItem.swift
//  Hoppy
//
//  Created by Sashucity on 9/5/16.
//  Copyright © 2016 Sashucity. All rights reserved.
//

import Foundation

class GameItem {
    var videoUrl: NSURL?
    var imageName: String
    
    init(imageName: String, urlString: String) {
        self.videoUrl = NSURL(string: urlString)
        self.imageName = imageName
    }
}