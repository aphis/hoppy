//
//  ViewController.swift
//  Hoppy
//
//  Created by Sashucity on 9/4/16.
//  Copyright © 2016 Sashucity. All rights reserved.
//

import UIKit

class GameViewController: UIViewController {
    @IBOutlet weak var boardContainerView: UIView!
    @IBOutlet weak var imageView: UIImageView!
    
    let videoSegue = "videoSegue"
    
    let tilesCountInRow = 3
    var board: GameBoard!
    var tileViews = [TileView]()
    var isSwipeEnabled = false
    
    var gameItem: GameItem!
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == videoSegue {
            let videoController = segue.destinationViewController as! VideoViewController
            videoController.gameItem = gameItem
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        board = GameBoard(rowsCount: tilesCountInRow)
        
        let image = UIImage(named: gameItem.imageName)!
        let imagesArray = image.tileImages(tilesCountInRow)
        
        imageView.image = image
        
        for tile in board.tiles {
            let tileView = TileView(frame: tileRect(tile.position),
                                    tile: tile,
                                    image: imagesArray[tile.index - 1])
            tileViews.append(tileView)
            boardContainerView.addSubview(tileView)
        }
        
        let leftSwipeRecognizer = UISwipeGestureRecognizer(target: self, action: #selector(GameViewController.handleSwipes(_:)))
        leftSwipeRecognizer.direction = .Left
        view.addGestureRecognizer(leftSwipeRecognizer)
        let rightSwipeRecognizer = UISwipeGestureRecognizer(target: self, action: #selector(GameViewController.handleSwipes(_:)))
        rightSwipeRecognizer.direction = .Right
        view.addGestureRecognizer(rightSwipeRecognizer)
        let upSwipeRecognizer = UISwipeGestureRecognizer(target: self, action: #selector(GameViewController.handleSwipes(_:)))
        upSwipeRecognizer.direction = .Up
        view.addGestureRecognizer(upSwipeRecognizer)
        let downSwipeRecognizer = UISwipeGestureRecognizer(target: self, action: #selector(GameViewController.handleSwipes(_:)))
        downSwipeRecognizer.direction = .Down
        view.addGestureRecognizer(downSwipeRecognizer)
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        setTileIndexesHidden(true)
        UIView.animateWithDuration(1.0, delay: 1.0, options: .CurveEaseOut, animations: {
            self.imageView.alpha = 0.0
            }, completion: { finished in
                self.setTileIndexesHidden(false)
                self.shuffle()
        })
    }

    func tileRect(position: TilePosition)-> CGRect
    {
        let padding: CGFloat = 5
        let totalPadding = CGFloat(board.rowsCount + 1) * padding
        let width = (CGFloat(boardContainerView.bounds.width) - totalPadding) / CGFloat(board.rowsCount)
        
        let x = CGFloat(position.column) * padding + (CGFloat)(position.column - 1) * width
        let y = CGFloat(position.row) * padding + (CGFloat)(position.row - 1) * width
        
        return CGRectMake(x, y, width, width)
    }
    
    func tileViewFor(tile: Tile)-> TileView {
        return tileViews.filter() { $0.tile.index == tile.index }.first!
    }

    func handleSwipes(sender: UISwipeGestureRecognizer) {
        guard isSwipeEnabled else { return }
        
        var tileDirection: Direction = .Left
        switch sender.direction {
        case UISwipeGestureRecognizerDirection.Right:
            tileDirection = .Left
        case UISwipeGestureRecognizerDirection.Left:
            tileDirection = .Right
        case UISwipeGestureRecognizerDirection.Up:
            tileDirection = .Down
        case UISwipeGestureRecognizerDirection.Down:
            tileDirection = .Up
        default:
            break
        }
        
        if let tileToMove = board.neighbourToEmptyTile(tileDirection) {
            UIView.animateWithDuration(0.3, delay: 0.0, options: .CurveEaseOut, animations: {
                let tileView = self.tileViewFor(tileToMove)
                tileView.frame = self.tileRect(self.board.emptyTilePosition)
                }, completion: nil)
            board.moveTileFrom(tileDirection)
        }
        
        if board.isSolved() {
            completeLevel()
        }
    }
    
    func shuffle() {
        let speed = 0.08
        var delay = 0.0
        
        let iterationsCount = 42 //+ random() % 5
        
        for i in 0..<iterationsCount {
            if i % 2 == 0 {
                var randomRow = random() % 2 == 1 ? board.emptyTilePosition.row - 1 : board.emptyTilePosition.row + 1
                if randomRow < 1 {
                    randomRow = board.emptyTilePosition.row + 1
                }
                if randomRow > tilesCountInRow {
                    randomRow = board.emptyTilePosition.row - 1
                }
                
                let tilePosition = TilePosition(row: randomRow, column: board.emptyTilePosition.column)
                let tileToMove = board.tileAt(tilePosition)!
                UIView.animateWithDuration(speed, delay: delay, options: .CurveEaseOut, animations: {
                    let tileView = self.tileViewFor(tileToMove)
                    tileView.frame = self.tileRect(self.board.emptyTilePosition)
                    }, completion: nil)
                board.moveTileFrom(randomRow < board.emptyTilePosition.row ? .Up : .Down)
            } else {
                var randomColumn = random() % 3 == 1 ? board.emptyTilePosition.column - 1 : board.emptyTilePosition.column + 1
                if randomColumn < 1 {
                    randomColumn = board.emptyTilePosition.column + 1
                }
                if randomColumn > tilesCountInRow {
                    randomColumn = board.emptyTilePosition.column - 1
                }
                
                let tilePosition = TilePosition(row: board.emptyTilePosition.row, column: randomColumn)
                let tileToMove = board.tileAt(tilePosition)!
                UIView.animateWithDuration(speed, delay: delay, options: .CurveEaseOut, animations: {
                    let tileView = self.tileViewFor(tileToMove)
                    tileView.frame = self.tileRect(self.board.emptyTilePosition)
                    }, completion: nil)
                board.moveTileFrom(randomColumn > board.emptyTilePosition.column ? .Right : .Left)
            }
            
            delay += speed
        }
        
        dispatch_after(
            dispatch_time(DISPATCH_TIME_NOW, Int64(delay * Double(NSEC_PER_SEC))),
            dispatch_get_main_queue(), {
                self.isSwipeEnabled = true
            }
        )
    }
    
    func setTileIndexesHidden(isHidden: Bool) {
        for tileView in tileViews {
            tileView.setIndexHidden(isHidden)
        }
    }

    func completeLevel() {
        isSwipeEnabled = false
        
        setTileIndexesHidden(true)
        
        UIView.animateWithDuration(1.0, delay: 0.0, options: .CurveEaseIn, animations: {
            self.imageView.alpha = 1.0
            }, completion: nil)
        
        dispatch_after(
            dispatch_time(DISPATCH_TIME_NOW, Int64(2.0 * Double(NSEC_PER_SEC))),
            dispatch_get_main_queue(), {
                let alertController = UIAlertController(title: "Congrats, my friend!", message: "You successfully completed the level! Now press continue to get your prize!", preferredStyle: .Alert)
                alertController.addAction(UIAlertAction(title: "Continue", style: .Default) {
                    (action) in
                    self.performSegueWithIdentifier(self.videoSegue, sender: nil)
                    })
                
                self.presentViewController(alertController, animated: true, completion: nil)
            }
        )
    }
}

