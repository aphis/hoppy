//
//  StartViewController.swift
//  Hoppy
//
//  Created by Sashucity on 9/5/16.
//  Copyright © 2016 Sashucity. All rights reserved.
//

import UIKit

class StartViewController: UIViewController, UINavigationControllerDelegate {
    let transitionAnimator = ZoomAnimator()
    let gameSegue = "gameSegue"
    var gameItems = [GameItem]()
    
    var selectedButton: UIButton?
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == gameSegue {
            let gameController = segue.destinationViewController as! GameViewController
            gameController.gameItem = gameItems[selectedButton!.tag]
            gameController.transitioningDelegate = transitionAnimator
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let gameItem1 = GameItem(imageName: "fifi", urlString: "http://player.ooyala.com/player/all/RtNGFjdDprUbXmOPa9oX2hEgyvp_G9gD.m3u8")
        let gameItem2 = GameItem(imageName: "bobTheBuilder", urlString: "http://player.ooyala.com/player/all/A4MTY0cDqOSJUWZe39tbjf0PZmdb37I8.m3u8")
        let gameItem3 = GameItem(imageName: "firemanSam", urlString: "http://player.ooyala.com/player/all/44cmU3MDE63NLA9qRIOlMv6U7kdDftOH.m3u8")
        let gameItem4 = GameItem(imageName: "pingu", urlString: "http://player.ooyala.com/player/all/gyMDk0cDr6snzZQW8Uz6vuF6wfHYi9eI.m3u8")
        gameItems = [gameItem1, gameItem2, gameItem3, gameItem4]
        
        navigationController?.delegate = self
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        
        navigationController?.delegate = nil
    }
    
    @IBAction func tapOnImage(sender: UIButton) {
        selectedButton = sender
        performSegueWithIdentifier(gameSegue, sender: nil)
    }
    
    func navigationController(
        navigationController: UINavigationController,
        animationControllerForOperation operation:
        UINavigationControllerOperation,
                                        fromViewController fromVC: UIViewController,
                                                           toViewController toVC: UIViewController
        ) -> UIViewControllerAnimatedTransitioning? {
        
        return ZoomAnimator()
    }

}
