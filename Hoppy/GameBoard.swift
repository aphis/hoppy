//
//  GameBoard.swift
//  Hoppy
//
//  Created by Sashucity on 9/4/16.
//  Copyright © 2016 Sashucity. All rights reserved.
//

import Foundation

enum Direction {
    case Left
    case Right
    case Up
    case Down
}

class GameBoard {
    var tiles = [Tile]()
    var emptyTilePosition: TilePosition
    
    var rowsCount: Int
    
    init(rowsCount: Int) {
        self.rowsCount = rowsCount
        self.emptyTilePosition = TilePosition(row: rowsCount, column: rowsCount)
        
        for index in 0..<rowsCount * rowsCount - 1 {
            let tilePosition = TilePosition(row: index / rowsCount + 1,
                                            column: index % rowsCount + 1)
            
            let tile = Tile(position: tilePosition, index: index + 1)
            tiles.append(tile)
        }
    }
    
    func tileAt(position: TilePosition) -> Tile? {
        for tile in self.tiles {
            if tile.position.row == position.row && tile.position.column == position.column {
                return tile
            }
        }
        
        return nil
    }
    
    func neighbourToEmptyTile(direction: Direction)-> Tile? {
        switch direction {
        case .Left:
            return tiles.filter() { $0.position.row == emptyTilePosition.row && emptyTilePosition.column - $0.position.column == 1 }.first ?? nil
        case .Right:
            return tiles.filter() { $0.position.row == emptyTilePosition.row && $0.position.column - emptyTilePosition.column == 1}.first ?? nil
        case .Up:
            return tiles.filter() { $0.position.column == emptyTilePosition.column && emptyTilePosition.row - $0.position.row == 1 }.last ?? nil
        case .Down:
            return tiles.filter() { $0.position.column == emptyTilePosition.column && $0.position.row -
                emptyTilePosition.row == 1}.first ?? nil
        }
    }
    
    func moveTileFrom(direction: Direction) {
        if let tile = neighbourToEmptyTile(direction) {
            let newEmptyTilePosition = tile.position
            tile.position = emptyTilePosition
            emptyTilePosition = newEmptyTilePosition
        }
    }
    
    func isSolved()-> Bool {
        for i in 0..<(rowsCount * rowsCount - 1)
        {
            let pos = TilePosition(row: i / rowsCount + 1,
                                   column: i % rowsCount + 1)
            if tiles[i].position != pos {
                return false
            }
        }
        return true
    }
}