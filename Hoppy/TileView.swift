//
//  TileView.swift
//  Hoppy
//
//  Created by Sashucity on 9/4/16.
//  Copyright © 2016 Sashucity. All rights reserved.
//

import UIKit

class TileView: UIView {
    var tile: Tile
    var position: TilePosition {
        get {
            return tile.position
        }
    }
    
    let imageView = UIImageView()
    let indexLabel = UILabel()
    
    required init(frame: CGRect, tile: Tile, image: UIImage) {
        self.tile = tile
        
        super.init(frame: frame)
        
        imageView.frame = bounds
        imageView.image = image
        addSubview(imageView)
        
        indexLabel.frame = bounds
        indexLabel.font = UIFont(name: "Avenir Next", size: 60.0)
        indexLabel.textAlignment = .Center
        indexLabel.textColor = UIColor(white: 0.08, alpha: 1.0)
        indexLabel.text = "\(tile.index)"
        addSubview(indexLabel)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError()
    }
    
    func setIndexHidden(isHidden: Bool) {
        indexLabel.hidden = isHidden
    }
}